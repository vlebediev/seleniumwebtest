import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FirstTest {

    @Test
    public void checkRozetkaIsOpen() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://rozetka.com.ua/");
        WebElement element = driver.findElement(By.xpath("//img[@alt='Rozetka Logo']"));
        boolean result = element.isDisplayed();
        System.out.println(result);
        Assert.assertTrue(result, "Rozetka is opened");

        driver.quit();
    }

    @Test
    public void testDBSourseDropdown() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://hrk-cv-qa-dbh2.axiomsl.us:8081/cv/ui/branch/US_Liquidity_Model/v1_5_0_QA2/configure?oType=DataSource&oName=5G_additionalFields");
        driver.findElement(By.xpath("//input[@id='username']"))
        .sendKeys("yourUserName");
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("yourPassword)");
        driver.findElement(By.xpath("//input[@value='LOG IN']")).click();
        driver.findElement(By.xpath("//div[@class='v-panel v-widget content v-panel-content cv-ds-db-source-combobox-panel v-panel-cv-ds-db-source-combobox-panel']//input[@class='v-filterselect-input v-filterselect-input-readonly']")).click();
    }

}
